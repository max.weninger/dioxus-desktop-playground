use crate::pages::model::{
    CurrentWeather, ForecastDay, ForecastTemperature, ForecastWeather, GpsData, WeatherDescription,
    WeatherError, WeatherResponse, UNDEFINED_CITY,
};
use chrono::Utc;
use serde_json::{from_str, json, Value};

fn get_default_location() -> GpsData {
    GpsData::new(47.8095, 13.0550, Some("Salzburg"))
}

async fn get_location_for_name_geonames(city: &String) -> Result<GpsData, WeatherError> {
    let url = format!("https://secure.geonames.org/searchJSON?name_startsWith={}&lang=de&username=omnijaws&maxRows=20", city);
    let res = reqwest::get(&url).await;
    match res {
        Ok(resp) => {
            let data = resp.text().await.unwrap();
            let v: Value = from_str(data.as_str()).unwrap_or_default();
            let geonames = v.get("geonames").unwrap().as_array();
            for location in geonames.unwrap().iter() {
                let location_data = json!(location);

                let city_geonames = location_data["name"].as_str().unwrap_or_default();
                let population = location_data["population"].as_i64().unwrap_or_default();
                if population == 0 {
                    continue;
                }
                let lat: f64 = location_data["lat"]
                    .as_str()
                    .unwrap_or_default()
                    .parse()
                    .unwrap_or_default();
                let lon: f64 = location_data["lng"]
                    .as_str()
                    .unwrap_or_default()
                    .parse()
                    .unwrap_or_default();
                let loc = GpsData {
                    lat,
                    lon,
                    city: city_geonames.to_string(),
                };
                return Ok(loc);
            }

            Ok(get_default_location())
        }
        Err(error) => Err(WeatherError::new(error.to_string().as_str())),
    }
}

async fn get_location_from_options(
    city: Option<String>,
    lat: Option<f64>,
    lon: Option<f64>,
) -> Result<GpsData, WeatherError> {
    let lat = lat.unwrap_or(f64::MAX);
    let lon = lon.unwrap_or(f64::MAX);
    let city = city.unwrap_or(UNDEFINED_CITY.to_string());

    let mut location = GpsData::new(lat, lon, None);

    if lat == f64::MAX && lon == f64::MAX && city == UNDEFINED_CITY {
        // TODO should be current location
        //let default_loc = get_this_location().await;
        let default_loc = get_default_location();
        location.lat = default_loc.lat;
        location.lon = default_loc.lon;
        location.city = default_loc.city;
    } else if city != UNDEFINED_CITY {
        let res = get_location_for_name_geonames(&city).await;
        match res {
            Ok(city_location) => {
                location.lat = city_location.lat;
                location.lon = city_location.lon;
                location.city = city_location.city;
            }
            Err(error) => {
                return Err(error);
            }
        }
    } else {
        // lat and lon must not be 0 aka undefined at this stage
        if lat == f64::MAX || lon == f64::MAX {
            return Err(WeatherError::new(
                "Either both lat and lon or city must be provided"
                    .to_string()
                    .as_str(),
            ));
        }
    }
    //println!("location = {}", location.to_string());
    Ok(location)
}

pub async fn fetch_weather_for_default_location() -> Result<WeatherResponse, WeatherError> {
    fetch_weather_for_city("Salzburg".to_string()).await
}

pub async fn fetch_weather_for_city(city: String) -> Result<WeatherResponse, WeatherError> {
    let res = get_location_from_options(Some(city), None, None).await;
    match res {
        Ok(location) => Ok(fetch_weather_met(location).await.unwrap_or_default()),
        Err(e) => Ok(WeatherResponse::default()),
    }
}

async fn fetch_weather_met(location: GpsData) -> Result<WeatherResponse, WeatherError> {
    let url = format!(
        "https://api.met.no/weatherapi/locationforecast/2.0/?lat={}&lon={}",
        location.lat, location.lon
    );

    let client = reqwest::Client::new();
    let res = client
        .get(url.clone())
        .send()
        .await;
    match res {
        Ok(resp) => {
            let data = resp.text().await.unwrap_or_default();
            let v: Value = from_str(data.as_str()).unwrap_or_default();

            let timeseries = json!(v["properties"]["timeseries"]);
            let weather = json!(timeseries[0]["data"]["instant"]["details"]);
            let symbol_code =
                json!(timeseries[0]["data"]["next_1_hours"]["summary"]["symbol_code"]);

            let mut dt = Utc::now().timestamp_millis() / 1000;

            let mut current = CurrentWeather {
                city: location.city.clone(),
                dt: dt.to_string(),
                temp: json!(weather["air_temperature"])
                    .as_f64()
                    .unwrap_or_default(),
                pressure: json!(weather["air_pressure_at_sea_level"])
                    .as_f64()
                    .unwrap_or_default() as i32,
                humidity: json!(weather["relative_humidity"])
                    .as_f64()
                    .unwrap_or_default() as i32,
                wind_speed: json!(weather["wind_speed"]).as_f64().unwrap_or_default(),
                weather: WeatherDescription {
                    weather_icons: symbol_code.as_str().unwrap_or_default().to_string(),
                    ..WeatherDescription::default()
                },
            };

            let mut days = vec![];
            for i in 0..6 {
                let day_dt = dt + (24 * 60 * 60 * i);
                let day_index = i as usize;

                let symbol_code =
                    json!(timeseries[day_index]["data"]["next_12_hours"]["summary"]["symbol_code"]);
                let temp_min = json!(
                    timeseries[day_index]["data"]["next_6_hours"]["details"]["air_temperature_min"]
                )
                .as_f64()
                .unwrap_or_default();
                let temp_max = json!(
                    timeseries[day_index]["data"]["next_6_hours"]["details"]["air_temperature_max"]
                )
                .as_f64()
                .unwrap_or_default();
                let forecast_day = ForecastDay {
                    dt: day_dt.to_string(),
                    weather: WeatherDescription {
                        weather_icons: symbol_code.as_str().unwrap_or_default().to_string(),
                        ..WeatherDescription::default()
                    },
                    temp: ForecastTemperature {
                        min: temp_min,
                        max: temp_max,
                    },
                };
                days.push(forecast_day);
            }
            let mut forecaset = ForecastWeather {
                city: location.city.to_string(),
                days,
            };

            let response = WeatherResponse {
                current,
                days: forecaset,
            };
            Ok(response)
        }
        Err(error) => Err(WeatherError::new(error.to_string().as_str())),
    }
}
