use std::error::Error;
use std::fmt;

use serde::{Deserialize, Deserializer, Serialize};
use serde_json::json;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct WeatherDescription {
    #[serde(default)]
    pub weather_icons: String,
}

impl Default for WeatherDescription {
    fn default() -> Self {
        WeatherDescription {
            weather_icons: "".to_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CurrentWeather {
    #[serde(default)]
    pub city: String,
    #[serde(deserialize_with = "string_from_long")]
    pub dt: String,
    pub temp: f64,
    pub pressure: i32,
    pub humidity: i32,
    pub wind_speed: f64,
    #[serde(deserialize_with = "from_array_first")]
    pub weather: WeatherDescription,
}

impl Default for CurrentWeather {
    fn default() -> Self {
        CurrentWeather {
            city: "".to_string(),
            dt: "".to_string(),
            temp: 0.0,
            pressure: 0,
            humidity: 0,
            wind_speed: 0.0,
            weather: WeatherDescription::default(),
        }
    }
}

impl PartialEq for CurrentWeather {
    fn eq(&self, other: &Self) -> bool {
        self.dt == other.dt
    }
}

impl CurrentWeather {
    pub fn error() -> Self {
        CurrentWeather {
            city: "city not found".to_string(),
            ..CurrentWeather::default()
        }
    }
}

fn string_from_long<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    let long: u64 = Deserialize::deserialize(deserializer)?;
    Ok(long.to_string())
}

fn from_array_first<'de, D>(deserializer: D) -> Result<WeatherDescription, D::Error>
where
    D: Deserializer<'de>,
{
    let arr: Vec<WeatherDescription> = Deserialize::deserialize(deserializer)?;
    Ok(arr[0].clone())
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ForecastTemperature {
    pub min: f64,
    pub max: f64,
}

impl Default for ForecastTemperature {
    fn default() -> Self {
        ForecastTemperature { min: 0.0, max: 0.0 }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ForecastDay {
    #[serde(deserialize_with = "string_from_long")]
    pub dt: String,
    #[serde(deserialize_with = "from_array_first")]
    pub weather: WeatherDescription,
    pub temp: ForecastTemperature,
}

impl Default for ForecastDay {
    fn default() -> Self {
        ForecastDay {
            dt: "".to_string(),
            weather: WeatherDescription::default(),
            temp: ForecastTemperature::default(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ForecastWeather {
    #[serde(skip_deserializing)]
    pub city: String,
    pub days: Vec<ForecastDay>,
}

impl Default for ForecastWeather {
    fn default() -> Self {
        ForecastWeather {
            city: "".to_string(),
            days: Vec::new(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct WeatherResponse {
    pub current: CurrentWeather,
    pub days: ForecastWeather,
}

impl Default for WeatherResponse {
    fn default() -> Self {
        WeatherResponse {
            current: CurrentWeather::default(),
            days: ForecastWeather::default(),
        }
    }
}

impl PartialEq for WeatherResponse {
    fn eq(&self, other: &Self) -> bool {
        self.current == other.current
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GpsData {
    pub lat: f64,
    pub lon: f64,
    #[serde(alias = "name")]
    pub city: String,
}

impl GpsData {
    pub(crate) fn new(lat: f64, lon: f64, city: Option<&str>) -> Self {
        GpsData {
            lat,
            lon,
            city: if city.is_some() {
                city.unwrap().to_string()
            } else {
                UNDEFINED_CITY.to_string()
            },
        }
    }
}

impl fmt::Display for GpsData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "lat = {} lon = {} city = {}",
            self.lat, self.lon, self.city
        )
    }
}

#[derive(Serialize, Deserialize)]
pub struct DayData {
    pub day_icon: String,
    pub day_name: String,
    pub day_temp_min: String,
    pub day_temp_max: String,
}

impl fmt::Display for DayData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", json!(self))
    }
}

#[derive(Debug)]
pub struct WeatherError {
    details: String,
}

impl WeatherError {
    pub(crate) fn new(msg: &str) -> WeatherError {
        WeatherError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for WeatherError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for WeatherError {}

pub const UNDEFINED_CITY: &str = "_undefined_";
