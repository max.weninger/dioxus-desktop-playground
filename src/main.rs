#![allow(non_snake_case)]

use dioxus::prelude::*;
use tracing::Level;

use crate::components::card::{Card, CardWithChildren};
use crate::components::card::Button;
use crate::components::clock::Clock;
use crate::components::current_weather::CurrentWeather;
use crate::pages::weather::{fetch_weather_for_city, fetch_weather_for_default_location};

mod components;
mod pages;

#[derive(Clone, Routable, Debug, PartialEq)]
enum Route {
    #[route("/")]
    Home {},
}

fn main() {
    // Init logger
    dioxus_logger::init(Level::INFO).expect("failed to init logger");

    let cfg = dioxus::desktop::Config::new()
        .with_custom_head(r#"<link rel="stylesheet" href="assets/tailwind.css">"#.to_string())
        .with_menu(None);
    LaunchBuilder::desktop().with_cfg(cfg).launch(App);
}

#[component]
fn App() -> Element {
    rsx! {
        Router::<Route> {}
    }
}

#[component]
fn Home() -> Element {
    let mut city = use_signal(|| "Salzburg".to_string());
    let current_weather_result = use_resource(move || fetch_weather_for_city(city.to_string()));
    rsx! {
        div { class: "bg-gradient-to-tl from-blue-800 to-blue-500 font-mono flex flex-col min-h-screen",
            div { class: "flex flex-col",
                div { class: "flex flex-row justify-center m-10 gap-4",

                    Card {
                        title: "Some useful title",
                        msg: "If I would know what I should say. Waiting for word wrapping",
                        button_text: "click",
                        button_href: "https://dioxuslabs.com/learn/0.5/reference/components"
                    }
                    CardWithChildren {
                        Clock{}
                    }
                    CardWithChildren { 

                        match &*current_weather_result.read_unchecked() {
                        Some(Ok(weather_response)) => {
                        let current_weather =weather_response.current.clone();
                        rsx! {
                        CurrentWeather {current_weather}
                        input {
                        class:"rounded-full py-2 px-8 placeholder:text-gray-400 ring-2 focus:ring-inset focus:ring-indigo-600",
                        placeholder:"City",
                        // we tell the component what to render
                        value: "{city}",
                        // and what to do when the value changes
                        onchange: move |event| city.set(event.value())
                        }
                        
                        
                        
                        }}
                        Some(Err(err)) => {
                        // if there was an error, render the error
                        rsx! {
                        div {class:"flex flex-col justify-center m-10 gap-4 min-h-[180px] min-w-[180px]",
                        p {class:"text-2xl text-center light:text-gray-900 dark:text-white","No weather data"}
                        }
                        }
                        }
                        None => {
                        // if the future is not resolved yet, render a loading message
                        rsx! {
                        div {class:"flex flex-col justify-center m-10 gap-4 min-h-[180px] min-w-[180px]",
                        p {class:"text-2xl text-center light:text-gray-900 dark:text-white","Loading..."}
                        }
                        }
                        }
                        }
                    }
                }
            }
        }
    }
}
