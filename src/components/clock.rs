use dioxus::prelude::*;
use std::fmt::Display;

use chrono::{DateTime, Local, Duration};

#[cfg(target_arch = "wasm32")]
use async_std::task::sleep;
#[cfg(not(target_arch = "wasm32"))]
use tokio::time::sleep;

#[derive(Debug, Clone)]
pub struct ClockTimer {
    current_time: DateTime<Local>,
}

impl ClockTimer {
    pub fn new() -> Self {
        let current_time = Local::now();
        Self {
            current_time,
        }
    }

    pub fn update(&mut self) {
        self.current_time = Local::now();
    }
}

impl Default for ClockTimer {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for ClockTimer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        ;
        write!(
            f,
            "{}",
            self.current_time.format("%H:%M:%S")
        )
    }
}

pub fn use_clock() -> Signal<ClockTimer> {
    let mut timer = use_signal(ClockTimer::new);

    use_future(move || async move {
        loop {
            timer.write().update();
            sleep(Duration::seconds(1).to_std().unwrap()).await;
        }
    });
    timer
}

#[component]
pub fn Clock() -> Element {
    let clock = use_clock();

    let time = clock.read().to_string();
    rsx! {
        div { class: "flex flex-col light:text-gray-900 dark:text-white items-center flex-1 mr-4 ml-4 w-30",
            div { class: "flex flex-row items-center flex-1",
                p { class: "text-4xl text-center", {time} }
            }
        }
    }
}