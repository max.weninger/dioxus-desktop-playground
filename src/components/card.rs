use dioxus::prelude::*;

// #[derive(PartialEq, Props, Clone)]
// pub struct CardProps {
//     title: String,
//     msg: String,
//     button_text: String,
//     button_href: String,
// }

#[component]
pub fn Button(button_text: String, button_href: String) -> Element {
    rsx! {

        a { href: { button_href },
            div { class: "text-xl inline-flex items-center px-3 py-2 font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800",

                {button_text},
                svg {
                    class: "rtl:rotate-180 w-3.5 h-3.5 ms-2",
                    xmlns: "http://www.w3.org/2000/svg",
                    fill: "none",
                    view_box: "0 0 14 10",

                    path { stroke: "currentColor", d: "M1 5h12m0 0L9 1m4 4L9 9" }
                }
            }
        }
    }
}

#[component]
pub fn Card(title: String, msg: String, button_text: String, button_href: String) -> Element {
    rsx! {
        CardWithChildren { 
            div { class: "text-2xl mb-2 font-bold tracking-tight light:text-gray-900 dark:text-white",
                {title}
            }
            p { class: "text-xl mb-3 font-normal light:text-gray-900 dark:text-white flex-1",
                {msg}
            }

            Button { button_text, button_href }
        }
    }
}

#[component]
pub fn CardWithChildren(children: Element) -> Element {
    rsx! {
        div { class: "max-w-prose p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 font-sans",
            div { class: "flex flex-col h-full", {children} }
        }
    }
}
