use chrono::Duration;
use dioxus::prelude::*;

use crate::pages::model::{CurrentWeather, ForecastDay};

#[component]
pub fn CurrentWeather(current_weather: CurrentWeather) -> Element {
    let image = "/assets/weathericons/{icon}.svg"
        .replace("{icon}", current_weather.weather.weather_icons.as_str());
    let dt: u64 = current_weather.dt.parse().unwrap_or_default();
    let d = std::time::UNIX_EPOCH + std::time::Duration::from_secs(dt);
    let datetime = chrono::prelude::DateTime::<chrono::Local>::from(d);
    let timestamp_str = datetime.format("%a, %e %b %Y %H:%M").to_string();
    let temp_str = format!("{}℃", current_weather.temp as i16);
    let wind_speed_str = format!("{}km/h", (current_weather.wind_speed * 3.6) as i16);
    let humidity_str = format!("{}%", current_weather.humidity);

    rsx! {
        div { class: "flex flex-col light:text-gray-900 dark:text-white",
            p { class: "text-3xl text-center", {current_weather.city} }
            p { class: "text-xl text-center", {timestamp_str} }
            div { class: "flex flex-row justify-center items-center gap-4",
                img { src: { image }, width: "120px" }
                div { class: "flex flex-col items-center gap-4",
                    p { class: "text-4xl text-center", {temp_str} }
                    div { class: "flex flex-row justify-center gap-4",
                        p { class: "text-xl text-center", {wind_speed_str} }
                        p { class: "text-xl text-center", {humidity_str} }
                    }
                }
            }
        }
    }
}
